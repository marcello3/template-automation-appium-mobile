# POC Automação Mobile Cucumber JS

Recomendado o uso do [Chocolatey](http://www.teste.com) no Windows para facilitar a instalação e configuração dos requisitos necessários para a automação.

Para instalar o Chocolatey:
Usando o Powershell em modo Administrativo:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

## Instalar o NodeJS (caso não tenha)

* Instalar via Chocolatey

```powershell
choco install nodejs
```

## Instalar JDK8

* Instalar via Chocolatey:

```powershell
choco install jdk8
```

ou

* Instalar através do site da [Oracle](https://www.oracle.com/technetwork/java/javase/downloads) (boa sorte!)

---

## Instalar Android SDK Tools

* Instalar via  Chocolatey:

```powershell
choco install android-sdk
```

ou

* Instalar através do site do [Android](https://developer.android.com/studio#downloads). (mais uma vez, boa sorte!)

### Configurar as variáveis de ambiente (considerando que foi instalado via Chocolatey)

```cmd
set ANDROID_HOME=C:\Android\android-sdk\
set ANDROID_SDK_ROOT=C:\Android\android-sdk\
set ANDROID_SDK_HOME=C:\Android\android-sdk\
set ANDROID_AVD_HOME=C:\Users\%username%\.android\avd
```

---

## Configurações com o sdkmanager

* Instalar emulator

```s
sdkmanager emulator
```

* Instalar Android 7 platforms
  
```s
sdkmanager platforms
```

* Instalar avd com sdkmanager

```s
sdkmanager emulator
```

* Instalar imagem de sistema do Android 7 (considerando que é o mínimo requerido para o projeto)

```s
sdkmanager "system-images;android-24;google_apis;x86_64"
```

---  
## Instalar appium

O Appium para a execução no pipeline, será utilizando a partir dos comandos configurados no package.json do projeto, ou seja instalado a partir do npm.

Para instalar o appium desktop que utilizarem para captura de objetos recomendo instalar a partir do [repositório oficial](https://github.com/appium/appium-desktop/releases/) do Appium!!!

---

## Criar avd tablet 7.1' polegadas com Android 7 (emulador android-7-tablet)

```s
avdmanager create avd -n "android-7-tablet" -k "system-images;android-24;google_apis;x86_64" -d 33
```
